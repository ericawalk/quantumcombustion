#!/usr/bin/env python
TChem_install_directory ='/projects/academic/ericwalk/software/install/tchem'

import sys
sys.path.append(TChem_install_directory+'/lib')
import pytchem
import numpy as np
import matplotlib.pyplot as plt
import time

# Pressure, Temperature, and mass fractions
one_atm = 101325 # [Pa]
T = 1200 # K
P = one_atm # Pa
Yp_fuel = 0.05 # mass fraction of H2
Yr_o2 = 0.45 # mass fraction of O2
Y_Ar = 0.5 # mass fraction of Ar

N = 1 # number of samples
Nvar = 5 # number of variables in sample
sample = np.zeros([N, Nvar])

# sample 1
sample[0, 0] = T # temperature
sample[0, 1] = P # pressure
sample[0, 2] = Yp_fuel # mass fraction of H2
sample[0, 3] = Yr_o2 # mass fraction of O2
sample[0, 4] = Y_Ar # mass fraction of Ar

pytchem.initialize()
tchem = pytchem.TChemDriver()
inputs_directory = TChem_install_directory + '/example/data/H2/'
tchem.createKineticModel('chem.inp', inputs_directory+'therm.dat')

Variables = ['T','P','H2','O2', 'AR']
indx=[]
for var in Variables:
    indx += [tchem.getStateVariableIndex(var)]

state = np.zeros([N, tchem.getLengthOfStateVector()])
for sp in range(N):
    state[sp,indx] = sample[sp,:]

tchem.setNumberOfSamples(N)
tchem.createStateVector()
tchem.setStateVector(state)

tchem.computeJacobianHomogeneousGasReactor()
jacobian1 = tchem.getJacobianHomogeneousGasReactor(0) # Extract Jacobian
print("Jacobian:", "\n", jacobian1)
np.save('Table_S1/jacobian.npy', jacobian1)

tchem.computeRHS_HomogeneousGasReactor()
RHS1 = tchem.getRHS_HomogeneousGasReactor(0).reshape(-1, 1) # Extract RHS of reactor model and cast as n by 1 np.array
print("RHS:", "\n", RHS1)
np.save('Table_S2/RHS.npy', RHS1)

state_vector1 = tchem.getStateVector(0)[2:].reshape(-1, 1) # Extract state vector and cast as n by 1 np.array
print("State Vector:", "\n", state_vector1)
np.save('Table_S3/state_vector.npy', state_vector1)

pytchem.finalize() # Finalize Kokkos. This deletes the TChem object and the data stored as Kokkos views