#!/bin/sh
export PYTHONPATH=/projects/academic/ericwalk/software/install/tchem/lib
module load gcc
module load python/py38-anaconda-2020.11
eval "$(/util/common/python/py38/anaconda3-2020.11/bin/conda shell.bash hook)"
conda activate tchem-custom
python pyTChemOneJacobian.py |& tee pyTChemOneJacobian.txt