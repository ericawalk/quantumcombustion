import qiskit
from qiskit import Aer, transpile, assemble
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.quantum_info import state_fidelity
from qiskit.aqua.algorithms import HHL, NumPyLSsolver
from qiskit.aqua.components.eigs import EigsQPE
from qiskit.aqua.components.reciprocals import LookupRotation
from qiskit.aqua.operators import MatrixOperator
from qiskit.aqua.components.initial_states import Custom
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.linalg import block_diag
import sys

def create_eigs(matrix, num_auxiliary, num_time_slices, negative_evals):
    ne_qfts = [None, None]
    if negative_evals:
        num_auxiliary += 1
        ne_qfts = [QFT(num_auxiliary - 1), QFT(num_auxiliary - 1).inverse()]

    return EigsQPE(MatrixOperator(matrix=matrix),
                  QFT(num_auxiliary).inverse(),
                  num_time_slices=num_time_slices,
                  num_ancillae=num_auxiliary,
                  expansion_mode='suzuki',
                  expansion_order=2,
                  evo_time=None,  # This is t, can set to: np.pi*3/4
                  negative_evals=negative_evals,
                  ne_qfts=ne_qfts)

def fidelity(hhl, ref):
    solution_hhl_normed = hhl / np.linalg.norm(hhl)
    solution_ref_normed = ref / np.linalg.norm(ref)
    fidelity = state_fidelity(solution_hhl_normed, solution_ref_normed)
    return fidelity

jacobian = np.load('../Table_S1/jacobian.npy')

# Replacement model
mean_block = np.vstack([[0, jacobian[1, 2], 0, jacobian[1, 9]], [jacobian[2, 1], 0, jacobian[2, 5], jacobian[2, 9]], [1, 1, 1, 0], [0, 0, 0, 1]])
mean_b = np.vstack([0, 0, 0.5, 0.5])

sol = np.matmul(np.linalg.inv(mean_block), mean_b)
print("Classical Solution:", "\n", sol)
print("\n")

# Execute Quantum Circuit without Preconditioner
orig_size = len(mean_b)
A_hhl, b_hhl, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(mean_block, mean_b.flatten())
eigs = create_eigs(A_hhl, 3, 50, False)
num_q, num_a = eigs.get_register_sizes()
init_state = Custom(num_q, state_vector=b_hhl)
reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)
algo = HHL(A_hhl, b_hhl, truncate_powerdim, truncate_hermitian, eigs, init_state, reci, num_q, num_a, orig_size)
circ = algo.construct_circuit(measurement=True)
result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
print("Quantum Solution with No Preconditioner:", "\n", np.round(result['solution'], 5))
print("\n")

print("Fidelity of Quantum Solution with No Preconditioner:", "\n", fidelity(result['solution'], sol))
print("\n")