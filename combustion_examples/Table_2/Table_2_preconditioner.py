import qiskit
from qiskit import Aer, transpile, assemble
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.quantum_info import state_fidelity
from qiskit.aqua.algorithms import HHL, NumPyLSsolver
from qiskit.aqua.components.eigs import EigsQPE
from qiskit.aqua.components.reciprocals import LookupRotation
from qiskit.aqua.operators import MatrixOperator
from qiskit.aqua.components.initial_states import Custom
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.linalg import block_diag
import sys

def create_eigs(matrix, num_auxiliary, num_time_slices, negative_evals):
    ne_qfts = [None, None]
    if negative_evals:
        num_auxiliary += 1
        ne_qfts = [QFT(num_auxiliary - 1), QFT(num_auxiliary - 1).inverse()]

    return EigsQPE(MatrixOperator(matrix=matrix),
                  QFT(num_auxiliary).inverse(),
                  num_time_slices=num_time_slices,
                  num_ancillae=num_auxiliary,
                  expansion_mode='suzuki',
                  expansion_order=2,
                  evo_time=None,  # This is t, can set to: np.pi*3/4
                  negative_evals=negative_evals,
                  ne_qfts=ne_qfts)

def fidelity(hhl, ref):
    solution_hhl_normed = hhl / np.linalg.norm(hhl)
    solution_ref_normed = ref / np.linalg.norm(ref)
    fidelity = state_fidelity(solution_hhl_normed, solution_ref_normed)
    return fidelity

jacobian = np.load('../Table_S1/jacobian.npy')

# Replacement model
mean_block = np.vstack([[0, jacobian[1, 2], 0, jacobian[1, 9]], [jacobian[2, 1], 0, jacobian[2, 5], jacobian[2, 9]], [1, 1, 1, 0], [0, 0, 0, 1]])
mean_b = np.vstack([0, 0, 0.5, 0.5])

sol = np.matmul(np.linalg.inv(mean_block), mean_b)
print("Classical Solution:", "\n", sol)
print("\n")

x_normalized = sol / np.linalg.norm(sol)

step_size = np.pi/8 # Step Size for Parameter Sweep
print("Step Size for Parameter Sweep:", "\n", 'pi/8')
print("\n")

# Parameter Sweep for Best Preconditioner
indices = []
preconditioners = []

for i in range(16):
    theta_i = i*step_size
    P_i = np.eye(4)
    P_i[0][0] = np.cos(theta_i)
    P_i[0][1] = -np.sin(theta_i)
    P_i[1][0] = np.sin(theta_i)
    P_i[1][1] = np.cos(theta_i)
    for j in range(16):
        theta_j = j*step_size
        P_j = np.eye(4)
        P_j[0][0] = np.cos(theta_j)
        P_j[0][2] = -np.sin(theta_j)
        P_j[2][0] = np.sin(theta_j)
        P_j[2][2] = np.cos(theta_j)
        P_j_P_i = np.matmul(P_j, P_i)
        for k in range(16):
            theta_k = k*step_size
            P_k = np.eye(4)
            P_k[0][0] = np.cos(theta_k)
            P_k[0][3] = -np.sin(theta_k)
            P_k[3][0] = np.sin(theta_k)
            P_k[3][3] = np.cos(theta_k)
            P_k_P_j_P_i = np.matmul(P_k, P_j_P_i)
            for l in range(16):
                theta_l = l*step_size
                P_l = np.eye(4)
                P_l[1][1] = np.cos(theta_l)
                P_l[1][2] = -np.sin(theta_l)
                P_l[2][1] = np.sin(theta_l)
                P_l[2][2] = np.cos(theta_l)
                P_l_P_k_P_j_P_i = np.matmul(P_l, P_k_P_j_P_i)
                indices.append(str(i)+'.'+str(j)+'.'+str(k)+'.'+str(l))
                preconditioners.append(P_l_P_k_P_j_P_i)
                
angles = []
more_indices = []

lkji = 0
for P_lkji in preconditioners:
    for m in range(8*int(sys.argv[1]), 8*(int(sys.argv[1])+1)):
        theta_m = m*step_size
        P_m = np.eye(4)
        P_m[1][1] = np.cos(theta_m)
        P_m[1][3] = -np.sin(theta_m)
        P_m[3][1] = np.sin(theta_m)
        P_m[3][3] = np.cos(theta_m)
        P_mlkji = np.matmul(P_m, P_lkji)
        for n in range(8*int(sys.argv[2]), 8*(int(sys.argv[2])+1)):
            theta_n = n*step_size
            P_n = np.eye(4)
            P_n[2][2] = np.cos(theta_n)
            P_n[2][3] = -np.sin(theta_n)
            P_n[3][2] = np.sin(theta_n)
            P_n[3][3] = np.cos(theta_n)
            P_nmlkji = np.matmul(P_n, P_mlkji)
            Pb = np.matmul(P_nmlkji, mean_b)
            Pb_normalized = Pb / np.linalg.norm(Pb)
            angle = np.arccos(np.dot(Pb_normalized.flatten(), x_normalized.flatten()))
            angles.append(angle)
            more_indices.append(indices[lkji]+'.'+str(m)+'.'+str(n))
    lkji = lkji + 1
            
print(min(angles))
print(more_indices[angles.index(min(angles))])
optimal_theta_i, optimal_theta_j, optimal_theta_k, optimal_theta_l, optimal_theta_m, optimal_theta_n = step_size*np.array([int(optimal_theta) for optimal_theta in more_indices[angles.index(min(angles))].split('.')])

# Prepare Preconditioner with Optimal Parameters from Sweep
optimal_P_i = np.eye(4)
optimal_P_i[0][0] = np.cos(optimal_theta_i)
optimal_P_i[0][1] = -np.sin(optimal_theta_i)
optimal_P_i[1][0] = np.sin(optimal_theta_i)
optimal_P_i[1][1] = np.cos(optimal_theta_i)
optimal_P_j = np.eye(4)
optimal_P_j[0][0] = np.cos(optimal_theta_j)
optimal_P_j[0][2] = -np.sin(optimal_theta_j)
optimal_P_j[2][0] = np.sin(optimal_theta_j)
optimal_P_j[2][2] = np.cos(optimal_theta_j)
optimal_P_k = np.eye(4)
optimal_P_k[0][0] = np.cos(optimal_theta_k)
optimal_P_k[0][3] = -np.sin(optimal_theta_k)
optimal_P_k[3][0] = np.sin(optimal_theta_k)
optimal_P_k[3][3] = np.cos(optimal_theta_k)
optimal_P_l = np.eye(4)
optimal_P_l[1][1] = np.cos(optimal_theta_l)
optimal_P_l[1][2] = -np.sin(optimal_theta_l)
optimal_P_l[2][1] = np.sin(optimal_theta_l)
optimal_P_l[2][2] = np.cos(optimal_theta_l)
optimal_P_m = np.eye(4)
optimal_P_m[1][1] = np.cos(optimal_theta_m)
optimal_P_m[1][3] = -np.sin(optimal_theta_m)
optimal_P_m[3][1] = np.sin(optimal_theta_m)
optimal_P_m[3][3] = np.cos(optimal_theta_m)
optimal_P_n = np.eye(4)
optimal_P_n[2][2] = np.cos(optimal_theta_n)
optimal_P_n[2][3] = -np.sin(optimal_theta_n)
optimal_P_n[3][2] = np.sin(optimal_theta_n)
optimal_P_n[3][3] = np.cos(optimal_theta_n)

# Perform Preconditioning
optimal_P_ji = np.matmul(optimal_P_j, optimal_P_i)
optimal_P_kji = np.matmul(optimal_P_k, optimal_P_ji)
optimal_P_lkji = np.matmul(optimal_P_l, optimal_P_kji)
optimal_P_mlkji = np.matmul(optimal_P_m, optimal_P_lkji)
optimal_P = np.matmul(optimal_P_n, optimal_P_mlkji)
print(optimal_P)
mean_block = np.matmul(optimal_P, mean_block)
mean_b = np.matmul(optimal_P, mean_b)

# Execute Quantum Circuit with Preconditioner
orig_size = len(mean_b)
A_hhl, b_hhl, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(mean_block, mean_b.flatten())
eigs = create_eigs(A_hhl, 3, 50, False)
num_q, num_a = eigs.get_register_sizes()
init_state = Custom(num_q, state_vector=b_hhl)
reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)
algo = HHL(A_hhl, b_hhl, truncate_powerdim, truncate_hermitian, eigs, init_state, reci, num_q, num_a, orig_size)
circ=algo.construct_circuit(measurement=True)
result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
print("Quantum Solution with Best Preconditioner:", "\n", np.round(result['solution'], 5))
print("\n")

print("Fidelity of Quantum Solution with Best Preconditioner:", "\n", fidelity(result['solution'], sol))