#!/bin/sh
module load python/py38-anaconda-2020.11
pip install qiskit
dos2unix Table_2_parameter_sweep_SLURM.sh
sbatch Table_2_parameter_sweep_SLURM.sh 0 0
sbatch Table_2_parameter_sweep_SLURM.sh 0 1
sbatch Table_2_parameter_sweep_SLURM.sh 1 0
sbatch Table_2_parameter_sweep_SLURM.sh 1 1