import qiskit
from qiskit import Aer, transpile, assemble
from qiskit.circuit.library import QFT
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.quantum_info import state_fidelity
from qiskit.aqua.algorithms import HHL, NumPyLSsolver
from qiskit.aqua.components.eigs import EigsQPE
from qiskit.aqua.components.reciprocals import LookupRotation
from qiskit.aqua.operators import MatrixOperator
from qiskit.aqua.components.initial_states import Custom
import numpy as np
from math import cos, sin
import matplotlib.pyplot as plt

def create_eigs(matrix, num_auxiliary, num_time_slices, negative_evals):
    ne_qfts = [None, None]
    if negative_evals:
        num_auxiliary += 1
        ne_qfts = [QFT(num_auxiliary - 1), QFT(num_auxiliary - 1).inverse()]

    return EigsQPE(MatrixOperator(matrix=matrix),
                  QFT(num_auxiliary).inverse(),
                  num_time_slices=num_time_slices,
                  num_ancillae=num_auxiliary,
                  expansion_mode='suzuki',
                  expansion_order=2,
                  evo_time=None,  # This is t, can set to: np.pi*3/4
                  negative_evals=negative_evals,
                  ne_qfts=ne_qfts)

def fidelity(hhl, ref):
    solution_hhl_normed = hhl / np.linalg.norm(hhl)
    solution_ref_normed = ref / np.linalg.norm(ref)
    fidelity = state_fidelity(solution_hhl_normed, solution_ref_normed)
    return fidelity

def one_HHL(mean_block, mean_b):
    orig_size = len(mean_b)
    A_hhl, b_hhl, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(mean_block, mean_b.flatten())
    eigs = create_eigs(A_hhl, 3, 50, False)
    num_q, num_a = eigs.get_register_sizes()
    init_state = Custom(num_q, state_vector=b_hhl)
    reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)
    algo = HHL(A_hhl, b_hhl, truncate_powerdim, truncate_hermitian, eigs, init_state, reci, num_q, num_a, orig_size)
    circ=algo.construct_circuit(measurement=True)
    result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
    result = np.round(result['solution'], 5)
    return(result)
    
row_1 = np.array([[1], [0]])
mean_b = np.array([[1], [1]])/np.sqrt(2)
print('b:', '\n', mean_b)
print('\n')

fidelities = []
permuted_fidelities = []
for i in range(1, 16):
    angle = i*np.pi/16
    rot = np.array([[cos(angle), -sin(angle)], [sin(angle), cos(angle)]])
    row_2 = np.matmul(rot, row_1)
    mean_block = np.vstack([row_1.transpose(), row_2.transpose()])
    print('A_'+str(i)+':', '\n', mean_block)
    print('\n')
    sol = np.matmul(np.linalg.inv(mean_block), mean_b)
    print('Classical Solution for A_'+str(i)+':', "\n", sol)
    print('\n')
    result = one_HHL(mean_block, mean_b)
    print('Quantum Solution for A_'+str(i)+':', "\n", result)
    print('\n')
    fidelities.append(fidelity(result, sol))
    print('Fidelity of Quantum Solution for A_'+str(i)+':', "\n", fidelities[-1])
    print('\n')
    if i in [1, 2, 3, 4, 12, 13, 14, 15]:
        print('A_'+str(i)+' is not diagonally dominant')
        print('\n')
    else:
        mean_block = np.vstack([mean_block[1], mean_block[0]]) # Permute rows
        print('Permutation of A_'+str(i)+':', '\n', mean_block)
        print('\n')
        sol = np.matmul(np.linalg.inv(mean_block), mean_b)
        print('Classical Solution for Permutation of A_'+str(i)+':', "\n", sol)
        print('\n')
        result = one_HHL(mean_block, mean_b)
        print('Quantum Solution for Permutation of A_'+str(i)+':', "\n", result)
        print('\n')
        permuted_fidelities.append(fidelity(result, sol))
        print('Fidelity of Quantum Solution for Permutation of A_'+str(i)+':', "\n", permuted_fidelities[-1])
        print('\n')

plt.scatter([i*np.pi/16 for i in range(1, 16)], fidelities)
plt.xlabel('Angle')
plt.ylabel('Fidelity')
plt.savefig('Figure_S1_a.png')

plt.clf()

plt.scatter([i*np.pi/16 for i in range(5, 12)], [fidelities[i] for i in range(4, 11)], c='b', label='Diagonally Dominant Representation')
plt.scatter([i*np.pi/16 for i in range(5, 12)], permuted_fidelities, c='r', label='Permuted Representation')
plt.xlabel('Angle')
plt.ylabel('Fidelity')
plt.savefig('Figure_S1_b.png')