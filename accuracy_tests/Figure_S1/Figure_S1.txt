Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
b: 
 [[0.70710678]
 [0.70710678]]


A_1: 
 [[1.         0.        ]
 [0.98078528 0.19509032]]


Classical Solution for A_1: 
 [[0.70710678]
 [0.06964394]]


Figure_S1.py:21: DeprecationWarning: The package qiskit.aqua.operators is deprecated. It was moved/refactored to qiskit.opflow (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  return EigsQPE(MatrixOperator(matrix=matrix),
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/operators/legacy/op_converter.py:90: DeprecationWarning: The variable qiskit.aqua.aqua_globals is deprecated. It was moved/refactored to qiskit.utils.algorithm_globals (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  num_processes=aqua_globals.num_processes)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/quantum_instance.py:135: DeprecationWarning: The class qiskit.aqua.QuantumInstance is deprecated. It was moved/refactored to qiskit.utils.QuantumInstance (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  warn_class('aqua.QuantumInstance',
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_1: 
 [0.65098-0.j 0.61827-0.j]


Fidelity of Quantum Solution for A_1: 
 0.6226748414479539


A_1 is not diagonally dominant


A_2: 
 [[1.         0.        ]
 [0.92387953 0.38268343]]


Classical Solution for A_2: 
 [[0.70710678]
 [0.14065228]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_2: 
 [0.60899-0.j 0.60242-0.j]


Fidelity of Quantum Solution for A_2: 
 0.6963409110387756


A_2 is not diagonally dominant


A_3: 
 [[1.         0.        ]
 [0.83146961 0.55557023]]


Classical Solution for A_3: 
 [[0.70710678]
 [0.2144985 ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_3: 
 [0.58961-0.j 0.57139-0.j]


Fidelity of Quantum Solution for A_3: 
 0.7906936689646001


A_3 is not diagonally dominant


A_4: 
 [[1.         0.        ]
 [0.70710678 0.70710678]]


Classical Solution for A_4: 
 [[0.70710678]
 [0.29289322]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_4: 
 [0.56296-0.j 0.60587-0.j]


Fidelity of Quantum Solution for A_4: 
 0.8266773475423296


A_4 is not diagonally dominant


A_5: 
 [[1.         0.        ]
 [0.55557023 0.83146961]]


Classical Solution for A_5: 
 [[0.70710678]
 [0.37795645]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_5: 
 [0.55043-0.j 0.63632-0.j]


Fidelity of Quantum Solution for A_5: 
 0.871402854560482


Permutation of A_5: 
 [[0.55557023 0.83146961]
 [1.         0.        ]]


Classical Solution for Permutation of A_5: 
 [[0.70710678]
 [0.37795645]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_5: 
 [0.6172 -0.j 0.53389-0.j]


Fidelity of Quantum Solution for Permutation of A_5: 
 0.9514019655770343


A_6: 
 [[1.         0.        ]
 [0.38268343 0.92387953]]


Classical Solution for A_6: 
 [[0.70710678]
 [0.47247365]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_6: 
 [0.60042-0.j 0.61686-0.j]


Fidelity of Quantum Solution for A_6: 
 0.9566038857921727


Permutation of A_6: 
 [[0.38268343 0.92387953]
 [1.         0.        ]]


Classical Solution for Permutation of A_6: 
 [[0.70710678]
 [0.47247365]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_6: 
 [0.61506-0.j 0.59867-0.j]


Fidelity of Quantum Solution for Permutation of A_6: 
 0.9669380731585019


A_7: 
 [[1.         0.        ]
 [0.19509032 0.98078528]]


Classical Solution for A_7: 
 [[0.70710678]
 [0.58030754]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_7: 
 [0.69823-0.j 0.59101-0.j]


Fidelity of Quantum Solution for A_7: 
 0.9997689644335361


Permutation of A_7: 
 [[0.19509032 0.98078528]
 [1.         0.        ]]


Classical Solution for Permutation of A_7: 
 [[0.70710678]
 [0.58030754]]


Quantum Solution for Permutation of A_7: 
 [0.59414-0.j 0.70193-0.j]


Fidelity of Quantum Solution for Permutation of A_7: 
 0.9675418395587799


A_8: 
 [[1.000000e+00 0.000000e+00]
 [6.123234e-17 1.000000e+00]]


Classical Solution for A_8: 
 [[0.70710678]
 [0.70710678]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Quantum Solution for A_8: 
 [0.70711-0.j 0.70711-0.j]


Fidelity of Quantum Solution for A_8: 
 1.0000000000000004


Permutation of A_8: 
 [[6.123234e-17 1.000000e+00]
 [1.000000e+00 0.000000e+00]]


Classical Solution for Permutation of A_8: 
 [[0.70710678]
 [0.70710678]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_8: 
 [0.70711+0.j 0.70711+0.j]


Fidelity of Quantum Solution for Permutation of A_8: 
 1.0000000000000004


A_9: 
 [[ 1.          0.        ]
 [-0.19509032  0.98078528]]


Classical Solution for A_9: 
 [[0.70710678]
 [0.86161211]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_9: 
 [0.73673-0.j 0.83598-0.j]


Fidelity of Quantum Solution for A_9: 
 0.9987649358057152


Permutation of A_9: 
 [[-0.19509032  0.98078528]
 [ 1.          0.        ]]


Classical Solution for Permutation of A_9: 
 [[0.70710678]
 [0.86161211]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_9: 
 [0.83105-0.j 0.73239-0.j]


Fidelity of Quantum Solution for Permutation of A_9: 
 0.9742402433657673


A_10: 
 [[ 1.          0.        ]
 [-0.38268343  0.92387953]]


Classical Solution for A_10: 
 [[0.70710678]
 [1.05826008]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_10: 
 [0.82864-0.j 0.94914-0.j]


Fidelity of Quantum Solution for A_10: 
 0.9835347147150555


Permutation of A_10: 
 [[-0.38268343  0.92387953]
 [ 1.          0.        ]]


Classical Solution for Permutation of A_10: 
 [[0.70710678]
 [1.05826008]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_10: 
 [0.9207-0.j 0.8038-0.j]


Fidelity of Quantum Solution for Permutation of A_10: 
 0.9318911967567736


A_11: 
 [[ 1.          0.        ]
 [-0.55557023  0.83146961]]


Classical Solution for A_11: 
 [[0.70710678]
 [1.32290374]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_11: 
 [0.91896-0.j 1.08832-0.j]


Fidelity of Quantum Solution for A_11: 
 0.9564013201345352


Permutation of A_11: 
 [[-0.55557023  0.83146961]
 [ 1.          0.        ]]


Classical Solution for Permutation of A_11: 
 [[0.70710678]
 [1.32290374]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for Permutation of A_11: 
 [0.98944-0.j 0.83547-0.j]


Fidelity of Quantum Solution for Permutation of A_11: 
 0.8633149558976972


A_12: 
 [[ 1.          0.        ]
 [-0.70710678  0.70710678]]


Classical Solution for A_12: 
 [[0.70710678]
 [1.70710678]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_12: 
 [0.98995-0.j 1.18995-0.j]


Fidelity of Quantum Solution for A_12: 
 0.9119845771860646


A_12 is not diagonally dominant


A_13: 
 [[ 1.          0.        ]
 [-0.83146961  0.55557023]]


Classical Solution for A_13: 
 [[0.70710678]
 [2.33101866]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_13: 
 [-0.91562+0.j -0.64669+0.j]


Fidelity of Quantum Solution for A_13: 
 0.6227897799653307


A_13 is not diagonally dominant


A_14: 
 [[ 1.          0.        ]
 [-0.92387953  0.38268343]]


Classical Solution for A_14: 
 [[0.70710678]
 [3.55486585]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_14: 
 [-0.8201 +0.j -0.48459+0.j]


Fidelity of Quantum Solution for A_14: 
 0.44475942333763957


A_14 is not diagonally dominant


A_15: 
 [[ 1.          0.        ]
 [-0.98078528  0.19509032]]


Classical Solution for A_15: 
 [[0.70710678]
 [7.17937563]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Quantum Solution for A_15: 
 [-0.76943+0.j -0.59417+0.j]


Fidelity of Quantum Solution for A_15: 
 0.4703655152807322


A_15 is not diagonally dominant


