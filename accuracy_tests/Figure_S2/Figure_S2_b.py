import numpy as np
import matplotlib.pyplot as plt
from qiskit import Aer, transpile, assemble
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.aqua.algorithms import HHL, NumPyLSsolver
from qiskit.aqua.components.eigs import EigsQPE
from qiskit.aqua.components.initial_states import Custom
from qiskit.aqua.components.reciprocals import LookupRotation
from qiskit.aqua.operators import MatrixOperator
from qiskit.circuit.library import QFT
from qiskit.quantum_info import state_fidelity
from scipy.linalg import qr

def create_eigs(matrix, num_auxiliary, num_time_slices, negative_evals):
    ne_qfts = [None, None]
    if negative_evals:
        num_auxiliary += 1
        ne_qfts = [QFT(num_auxiliary - 1), QFT(num_auxiliary - 1).inverse()]

    return EigsQPE(MatrixOperator(matrix=matrix),
                  QFT(num_auxiliary).inverse(),
                  num_time_slices=num_time_slices,
                  num_ancillae=num_auxiliary,
                  expansion_mode='suzuki',
                  expansion_order=2,
                  evo_time=None,  # This is t, can set to: np.pi*3/4
                  negative_evals=negative_evals,
                  ne_qfts=ne_qfts)

def fidelity(hhl, ref):
    solution_hhl_normed = hhl / np.linalg.norm(hhl)
    solution_ref_normed = ref / np.linalg.norm(ref)
    fidelity = state_fidelity(solution_hhl_normed, solution_ref_normed)
    return fidelity

def one_HHL(mean_block, mean_b):
    orig_size = len(mean_b)
    A_hhl, b_hhl, truncate_powerdim, truncate_hermitian = HHL.matrix_resize(mean_block, mean_b.flatten())
    eigs = create_eigs(A_hhl, 3, 50, False)
    num_q, num_a = eigs.get_register_sizes()
    init_state = Custom(num_q, state_vector=b_hhl)
    reci = LookupRotation(negative_evals=eigs._negative_evals, evo_time=eigs._evo_time)
    algo = HHL(A_hhl, b_hhl, truncate_powerdim, truncate_hermitian, eigs, init_state, reci, num_q, num_a, orig_size)
    circ=algo.construct_circuit(measurement=True)
    result = algo.run(QuantumInstance(Aer.get_backend('statevector_simulator')))
    result = np.round(result['solution'], 5)
    return(result)
    
mean_b = np.array([[1], [1], [1], [1]])/np.sqrt(4)
print('b:', '\n', mean_b)
print('\n')

fidelities = []
np.random.seed(0)
for i in range(1, 8):
    mean_block = np.random.rand(4, 4)
    mean_block, R = qr(mean_block)
    print('A_'+str(i)+':', '\n', mean_block)
    print('\n')
    sol = np.matmul(np.linalg.inv(mean_block), mean_b)
    print('Classical Solution for A_'+str(i)+':', "\n", sol)
    print('\n')
    result = one_HHL(mean_block, mean_b)
    print('Quantum Solution for A_'+str(i)+':', "\n", result)
    print('\n')
    fidelities.append(fidelity(result, sol))
    print('Fidelity of Quantum Solution for A_'+str(i)+':', "\n", fidelities[-1])
    print('\n')
    
plt.hist(x = fidelities, bins = 'auto', rwidth = 0.9)
plt.ylabel('Frequency')
plt.xlabel('Fidelity')
plt.savefig('Figure_S2_b.png')