Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
A_1: 
 [[0.5488135  0.71518937]
 [0.60276338 0.54488318]]


b_1_1: 
 [[0.8877147 ]
 [0.81252447]]


Classical Solution for A_1 and b_1_1: 
 [[0.73765643]
 [0.67517627]]


Figure_S5_a.py:19: DeprecationWarning: The package qiskit.aqua.operators is deprecated. It was moved/refactored to qiskit.opflow (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  return EigsQPE(MatrixOperator(matrix=matrix),
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/operators/legacy/op_converter.py:90: DeprecationWarning: The variable qiskit.aqua.aqua_globals is deprecated. It was moved/refactored to qiskit.utils.algorithm_globals (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  num_processes=aqua_globals.num_processes)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/quantum_instance.py:135: DeprecationWarning: The class qiskit.aqua.QuantumInstance is deprecated. It was moved/refactored to qiskit.utils.QuantumInstance (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  warn_class('aqua.QuantumInstance',
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_1 and b_1_1: 
 [0.72663-0.j 0.68517-0.j]


Fidelity of Quantum Solution for A_1 and b_1_1: 
 0.9997799033727537


b_1_2: 
 [[ 0.08072101]
 [-0.07432747]]


Classical Solution for A_1 and b_1_2: 
 [[-0.73563968]
 [ 0.67737305]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_1 and b_1_2: 
 [ 0.72355-0.j -0.61486+0.j]


Fidelity of Quantum Solution for A_1 and b_1_2: 
 0.9984153573587892


A_2: 
 [[0.4236548  0.64589411]
 [0.43758721 0.891773  ]]


b_2_1: 
 [[-0.06769466]
 [ 0.03634923]]


Classical Solution for A_2 and b_2_1: 
 [[-0.88102323]
 [ 0.473073  ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_2 and b_2_1: 
 [-0.52671-0.j  0.23218+0.j]


Fidelity of Quantum Solution for A_2 and b_2_1: 
 0.9939920659833092


b_2_2: 
 [[-0.7693361 ]
 [-0.97068564]]


Classical Solution for A_2 and b_2_2: 
 [[-0.62113793]
 [-0.78370126]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_2 and b_2_2: 
 [0.63496-0.j 0.77609-0.j]


Fidelity of Quantum Solution for A_2 and b_2_2: 
 0.9997592077927473


A_3: 
 [[0.96366276 0.38344152]
 [0.79172504 0.52889492]]


b_3_1: 
 [[0.95709091]
 [0.9358462 ]]


Classical Solution for A_3 and b_3_1: 
 [[0.71499772]
 [0.69912679]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_3 and b_3_1: 
 [0.70236-0.j 0.72351-0.j]


Fidelity of Quantum Solution for A_3 and b_3_1: 
 0.9993213095200246


b_3_2: 
 [[-0.0658962 ]
 [ 0.13915037]]


Classical Solution for A_3 and b_3_2: 
 [[-0.42799549]
 [ 0.90378087]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_3 and b_3_2: 
 [-0.23878-0.j  0.64659+0.j]


Fidelity of Quantum Solution for A_3 and b_3_2: 
 0.9921851193492615


A_4: 
 [[0.56804456 0.92559664]
 [0.07103606 0.0871293 ]]


b_4_1: 
 [[0.67427588]
 [0.08091084]]


Classical Solution for A_4 and b_4_1: 
 [[0.99287723]
 [0.11914193]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_4 and b_4_1: 
 [0.97265-0.j 0.1316 -0.j]


Fidelity of Quantum Solution for A_4 and b_4_1: 
 0.9997732678784816


b_4_2: 
 [[ 0.02016723]
 [-0.01289835]]


Classical Solution for A_4 and b_4_2: 
 [[-0.84243587]
 [ 0.53879662]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_4 and b_4_2: 
 [-0.16059+0.j  0.07327-0.j]


Fidelity of Quantum Solution for A_4 and b_4_2: 
 0.9802602632189384


A_5: 
 [[0.0202184  0.83261985]
 [0.77815675 0.87001215]]


b_5_1: 
 [[ 0.4018062 ]
 [-0.23419313]]


Classical Solution for A_5 and b_5_1: 
 [[-0.86396023]
 [ 0.50356005]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_5 and b_5_1: 
 [ 0.86986-0.j -0.49494+0.j]


Fidelity of Quantum Solution for A_5 and b_5_1: 
 0.999891635492165


b_5_2: 
 [[-0.71718925]
 [-1.14999668]]


Classical Solution for A_5 and b_5_2: 
 [[-0.52917168]
 [-0.84851478]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_5 and b_5_2: 
 [0.52765-0.j 0.84938-0.j]


Fidelity of Quantum Solution for A_5 and b_5_2: 
 0.999996940508465


A_6: 
 [[0.97861834 0.79915856]
 [0.46147936 0.78052918]]


b_6_1: 
 [[1.25565926]
 [0.81116824]]


Classical Solution for A_6 and b_6_1: 
 [[0.8399718 ]
 [0.54263005]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_6 and b_6_1: 
 [0.84405-0.j 0.53863-0.j]


Fidelity of Quantum Solution for A_6 and b_6_1: 
 0.9999690217820693


b_6_2: 
 [[-0.19702478]
 [ 0.17611691]]


Classical Solution for A_6 and b_6_2: 
 [[-0.74555793]
 [ 0.66644082]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_6 and b_6_2: 
 [ 0.73998-0.j -0.64437+0.j]


Fidelity of Quantum Solution for A_6 and b_6_2: 
 0.9998314788365289


A_7: 
 [[0.11827443 0.63992102]
 [0.14335329 0.94466892]]


b_7_1: 
 [[-0.01893085]
 [ 0.00293221]]


Classical Solution for A_7 and b_7_1: 
 [[-0.98821604]
 [ 0.15306551]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_7 and b_7_1: 
 [-0.05501+0.j -0.00785+0.j]


Fidelity of Quantum Solution for A_7 and b_7_1: 
 0.9152398909350992


b_7_2: 
 [[-0.59362059]
 [-0.85854842]]


Classical Solution for A_7 and b_7_2: 
 [[-0.56871827]
 [-0.82253239]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_7 and b_7_2: 
 [0.57633-0.j 0.8213 -0.j]


Fidelity of Quantum Solution for A_7 and b_7_2: 
 0.9999518556903068


A_8: 
 [[0.52184832 0.41466194]
 [0.26455561 0.77423369]]


b_8_1: 
 [[-0.25721303]
 [ 0.14157911]]


Classical Solution for A_8 and b_8_1: 
 [[-0.87605487]
 [ 0.48221144]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_8 and b_8_1: 
 [ 0.81594-0.j -0.5673 +0.j]


Fidelity of Quantum Solution for A_8 and b_8_1: 
 0.9891467539590114


b_8_2: 
 [[-0.65485288]
 [-0.75903246]]


Classical Solution for A_8 and b_8_2: 
 [[-0.65323403]
 [-0.75715606]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_8 and b_8_2: 
 [0.72774-0.j 0.70973-0.j]


Fidelity of Quantum Solution for A_8 and b_8_2: 
 0.9926087614372188


A_9: 
 [[0.45615033 0.56843395]
 [0.0187898  0.6176355 ]]


b_9_1: 
 [[-0.40415773]
 [ 0.03583924]]


Classical Solution for A_9 and b_9_1: 
 [[-0.99609129]
 [ 0.08832977]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_9 and b_9_1: 
 [ 0.93195+0.j -0.0341 -0.j]


Fidelity of Quantum Solution for A_9 and b_9_1: 
 0.9973117696329145


b_9_2: 
 [[-0.62596628]
 [-0.23333783]]


Classical Solution for A_9 and b_9_2: 
 [[-0.93701613]
 [-0.34928608]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_9 and b_9_2: 
 [0.9951 -0.j 0.31515-0.j]


Fidelity of Quantum Solution for A_9 and b_9_2: 
 0.9974920096087252


A_10: 
 [[0.61209572 0.616934  ]
 [0.94374808 0.6818203 ]]


b_10_1: 
 [[ 0.07550437]
 [-0.08921653]]


Classical Solution for A_10 and b_10_1: 
 [[-0.64600934]
 [ 0.7633295 ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_10 and b_10_1: 
 [-0.27035+0.j  0.44216-0.j]


Fidelity of Quantum Solution for A_10 and b_10_1: 
 0.9766031562402777


b_10_2: 
 [[-0.86241467]
 [-1.11650395]]


Classical Solution for A_10 and b_10_2: 
 [[-0.61129731]
 [-0.79140103]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_10 and b_10_2: 
 [0.63494-0.j 0.76248-0.j]


Fidelity of Quantum Solution for A_10 and b_10_2: 
 0.99865493797748


A_11: 
 [[0.3595079  0.43703195]
 [0.6976312  0.06022547]]


b_11_1: 
 [[0.5622272 ]
 [0.54345817]]


Classical Solution for A_11 and b_11_1: 
 [[0.71900635]
 [0.6950035 ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_11 and b_11_1: 
 [0.9026 -0.j 0.26657-0.j]


Fidelity of Quantum Solution for A_11 and b_11_1: 
 0.7857329487225142


b_11_2: 
 [[ 0.18761925]
 [-0.30983882]]


Classical Solution for A_11 and b_11_2: 
 [[-0.51797488]
 [ 0.85539583]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_11 and b_11_2: 
 [ 0.20934-0.j -0.97663+0.j]


Fidelity of Quantum Solution for A_11 and b_11_2: 
 0.892947167469746


A_12: 
 [[0.66676672 0.67063787]
 [0.21038256 0.1289263 ]]


b_12_1: 
 [[0.82625895]
 [0.23783662]]


Classical Solution for A_12 and b_12_1: 
 [[0.96098056]
 [0.2766159 ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_12 and b_12_1: 
 [0.98463-0.j 0.25231-0.j]


Fidelity of Quantum Solution for A_12 and b_12_1: 
 0.9991347209820269


b_12_2: 
 [[ 0.04334747]
 [-0.04724142]]


Classical Solution for A_12 and b_12_2: 
 [[-0.67608764]
 [ 0.73682121]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_12 and b_12_2: 
 [-0.14352+0.j  0.23829-0.j]


Fidelity of Quantum Solution for A_12 and b_12_2: 
 0.9603990671691428


A_13: 
 [[0.31542835 0.36371077]
 [0.57019677 0.43860151]]


b_13_1: 
 [[ 0.05567603]
 [-0.06091825]]


Classical Solution for A_13 and b_13_1: 
 [[-0.67463285]
 [ 0.73815345]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_13 and b_13_1: 
 [ 0.56766-0.j -0.71792+0.j]


Fidelity of Quantum Solution for A_13 and b_13_1: 
 0.994907535744528


b_13_2: 
 [[-0.47877992]
 [-0.68600195]]


Classical Solution for A_13 and b_13_2: 
 [[-0.57232141]
 [-0.82002939]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_13 and b_13_2: 
 [0.60708-0.j 0.78034-0.j]


Fidelity of Quantum Solution for A_13 and b_13_2: 
 0.997316254037606


A_14: 
 [[0.98837384 0.10204481]
 [0.20887676 0.16130952]]


b_14_1: 
 [[0.98424734]
 [0.24127611]]


Classical Solution for A_14 and b_14_1: 
 [[0.97124349]
 [0.23808838]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_14 and b_14_1: 
 [0.96201-0.j 0.30555-0.j]


Fidelity of Quantum Solution for A_14 and b_14_1: 
 0.9954987271678251


b_14_2: 
 [[-0.01620679]
 [ 0.13532748]]


Classical Solution for A_14 and b_14_2: 
 [[-0.11891006]
 [ 0.99290503]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Quantum Solution for A_14 and b_14_2: 
 [ 0.17871-0.j -0.88555+0.j]


Fidelity of Quantum Solution for A_14 and b_14_2: 
 0.9936231703702241


