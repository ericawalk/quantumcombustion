#!/bin/sh
##SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
#SBATCH --constraint=MRI|NIH #request 'cascade' and 'skylake' nodes
#SBATCH --time=72:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
##SBATCH --constraint=IB
#SBATCH --mem=128000
##SBATCH --mem=23000
#SBATCH --job-name=quantum_CO_Ox_UQ
#SBATCH --output=job_b.out
#SBATCH --mail-user=ajbecerr@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
python Figure_S5_b.py |& tee Figure_S5_b.txt