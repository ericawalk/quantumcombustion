#!/bin/sh
##SBATCH --constraint=UBHPC&CPU-Gold-6130&INTEL&u25&OPA&MRI #request 'skylake' nodes
##SBATCH --constraint=MRI|NIH #request 'cascade' and 'skylake' nodes
#SBATCH --constraint=CPU-Gold-6130
## alternate: #SBATCH --constraint=V100
#SBATCH --partition=debug
#SBATCH --qos=debug
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=16
##SBATCH --constraint=IB
#SBATCH --mem=187000
##SBATCH --mem=23000
#SBATCH --job-name=quantum_CO_Ox_UQ
#SBATCH --output=job_a.out
#SBATCH --mail-user=ajbecerr@buffalo.edu
#SBATCH --mail-type=ALL
##SBATCH --requeue
python Figure_S4_a.py |& tee Figure_S4_a.txt