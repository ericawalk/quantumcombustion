#!/bin/sh
module load python/py38-anaconda-2020.11
pip install qiskit
dos2unix Figure_S6_a_SLURM.sh
dos2unix Figure_S6_b_SLURM.sh
sbatch Figure_S6_a_SLURM.sh
sbatch Figure_S6_b_SLURM.sh