Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
b: 
 [[1]
 [0]]


A_1: 
 [[0.5488135  0.71518937]
 [0.         0.54488318]]


Classical Solution for A_1: 
 [[1.8221126]
 [0.       ]]


Figure_S6_a.py:19: DeprecationWarning: The package qiskit.aqua.operators is deprecated. It was moved/refactored to qiskit.opflow (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  return EigsQPE(MatrixOperator(matrix=matrix),
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/operators/legacy/op_converter.py:90: DeprecationWarning: The variable qiskit.aqua.aqua_globals is deprecated. It was moved/refactored to qiskit.utils.algorithm_globals (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  num_processes=aqua_globals.num_processes)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/quantum_instance.py:135: DeprecationWarning: The class qiskit.aqua.QuantumInstance is deprecated. It was moved/refactored to qiskit.utils.QuantumInstance (pip install qiskit-terra). For more information see <https://github.com/Qiskit/qiskit-aqua/blob/master/README.md#migration-guide>
  warn_class('aqua.QuantumInstance',
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_1: 
 [-2.04838+0.j  1.80873-0.j]


Fidelity of Quantum Solution for A_1: 
 0.5618930795293926


A_2: 
 [[0.4236548  0.64589411]
 [0.         0.891773  ]]


Classical Solution for A_2: 
 [[2.3604123]
 [0.       ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_2: 
 [2.14426-0.j 0.13115-0.j]


Fidelity of Quantum Solution for A_2: 
 0.9962729942760468


A_3: 
 [[0.96366276 0.38344152]
 [0.         0.52889492]]


Classical Solution for A_3: 
 [[1.03770742]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_3: 
 [1.01425-0.j 0.05773+0.j]


Fidelity of Quantum Solution for A_3: 
 0.9967707003475377


A_4: 
 [[0.56804456 0.92559664]
 [0.         0.0871293 ]]


Classical Solution for A_4: 
 [[1.76042527]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_4: 
 [ 1.89161-0.j -0.08054+0.j]


Fidelity of Quantum Solution for A_4: 
 0.9981904380596459


A_5: 
 [[0.0202184  0.83261985]
 [0.         0.87001215]]


Classical Solution for A_5: 
 [[49.45990418]
 [ 0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_5: 
 [1.62311-0.j 0.81133-0.j]


Fidelity of Quantum Solution for A_5: 
 0.8000887161098187


A_6: 
 [[0.97861834 0.79915856]
 [0.         0.78052918]]


Classical Solution for A_6: 
 [[1.02184882]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_6: 
 [0.99713-0.j 0.02993+0.j]


Fidelity of Quantum Solution for A_6: 
 0.99909984197341


A_7: 
 [[0.11827443 0.63992102]
 [0.         0.94466892]]


Classical Solution for A_7: 
 [[8.45491316]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_7: 
 [3.00511-0.j 0.66291-0.j]


Fidelity of Quantum Solution for A_7: 
 0.9535962653229538


A_8: 
 [[0.52184832 0.41466194]
 [0.         0.77423369]]


Classical Solution for A_8: 
 [[1.91626562]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_8: 
 [ 2.1735 -0.j -0.62849+0.j]


Fidelity of Quantum Solution for A_8: 
 0.9228380904514859


A_9: 
 [[0.45615033 0.56843395]
 [0.         0.6176355 ]]


Classical Solution for A_9: 
 [[2.19225972]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_9: 
 [1.54218-0.j 0.45179-0.j]


Fidelity of Quantum Solution for A_9: 
 0.9209605143976927


A_10: 
 [[0.61209572 0.616934  ]
 [0.         0.6818203 ]]


Classical Solution for A_10: 
 [[1.6337314]
 [0.       ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_10: 
 [ 2.14418-0.j -0.70752+0.j]


Fidelity of Quantum Solution for A_10: 
 0.9018094399071201


A_11: 
 [[0.3595079  0.43703195]
 [0.         0.06022547]]


Classical Solution for A_11: 
 [[2.78158004]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_11: 
 [ 4.00208-0.j -1.00822+0.j]


Fidelity of Quantum Solution for A_11: 
 0.9403218162628887


A_12: 
 [[0.66676672 0.67063787]
 [0.         0.1289263 ]]


Classical Solution for A_12: 
 [[1.49977492]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_12: 
 [1.46674-0.j 0.03283-0.j]


Fidelity of Quantum Solution for A_12: 
 0.9994992534479016


A_13: 
 [[0.31542835 0.36371077]
 [0.         0.43860151]]


Classical Solution for A_13: 
 [[3.17029207]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_13: 
 [2.7464 -0.j 0.33735-0.j]


Fidelity of Quantum Solution for A_13: 
 0.9851361907906288


A_14: 
 [[0.98837384 0.10204481]
 [0.         0.16130952]]


Classical Solution for A_14: 
 [[1.01176292]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Input matrix is not hermitian. It will be expanded to a hermitian matrix automatically.
Quantum Solution for A_14: 
 [ 1.01791-0.j -0.05997-0.j]


Fidelity of Quantum Solution for A_14: 
 0.9965410480578865


A_15: 
 [[0.65310833 0.2532916 ]
 [0.         0.24442559]]


Classical Solution for A_15: 
 [[1.53113957]
 [0.        ]]


/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:79: DeprecationWarning: The Custom class is deprecated as of Aqua 0.9 and will be removed no earlier than 3 months after the release date. Instead, all algorithms and circuits accept a plain QuantumCircuit. Custom(state_vector=vector) is the same as a circuit where the ``initialize(vector/np.linalg.norm(vector))`` method has been called.
  super().__init__()
/user/ajbecerr/.local/lib/python3.8/site-packages/qiskit/aqua/components/initial_states/custom.py:151: DeprecationWarning: The StateVectorCircuit class is deprecated as of Qiskit Aqua 0.9.0 and will be removed no earlier than 3 months after the release. If you need to initialize a circuit, use the QuantumCircuit.initialize or QuantumCircuit.isometry methods. For a parameterized initialization, try the qiskit.ml.circuit.library.RawFeatureVector class.
  svc = StateVectorCircuit(self._state_vector)
Quantum Solution for A_15: 
 [ 1.5493 -0.j -0.04709+0.j]


Fidelity of Quantum Solution for A_15: 
 0.9990770348235823


