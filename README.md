# Repo Contents:

- 'accuracy_tests/': Directory for HHL accuracy tests of random linear systems with various criteria resticted.
    - 'Figure_2/': Directory for accuracy test(s) with random 2 x 2 matrices A and uniform RHS vector b = [1, 1]^T / sqrt(2).
    - 'Figure_3/': Directory for accuracy test(s) with random 4 x 4 matrices A and uniform RHS vector b = [1, 1, 1, 1]^T / 2.
    - 'Figure_S1/': Directory for accuracy test(s) with select 2 x 2 matrices A and uniform RHS vector b = [1, 1]^T / sqrt(2).
    - 'Figure_S2/': Directory for accuracy test(s) with random 2 x 2 and 4 x 4 orthogonal matrices A and uniform RHS vector b = [1, 1]^T / sqrt(2) or [1, 1, 1, 1]^T / 2.
    - 'Figure_S3/': Directory for accuracy test(s) with random 2 x 2 orthonormal matrices A and uniform RHS vector b = [1, 1]^T / sqrt(2).
    - 'Figure_S4/': Directory for accuracy test(s) with random 2 x 2 and 4 x 4 diagonal matrices A and uniform RHS vector b = [1, 1]^T / sqrt(2) or [1, 1, 1, 1]^T / 2.
    - 'Figure_S5/': Directory for accuracy test(s) with random 2 x 2 and 4 x 4 matrices A and RHS vectors lambda*v (any eigenvalue-eigenvector pair of A).
    - 'Figure_S6/': Directory for accuracy test(s) with select random 2 x 2 and 4 x 4 matrices A and RHS vectors v = [1, 0]^T or [1, 0, 0, 0]^T (where v is an eigenvector of A).

- 'combustion_examples/': Directory for combustion models and their solution(s).
    - 'Table_1/': Directory for combustion example with H2, O2, and Ar. 
    - 'Table_2/': Directory for combustion example with H2, O2, H2O, and Ar.
    - 'Table_S1/': Directory with np.array containing entries of matrix A for Jacobian linearized model. 
    - 'Table_S2/': Directory with np.array containing entries of RHS vector for Jacobian linearized model. 
    - 'Table_S3/': Directory with np.array containing entries of initial statevector for Jacobian linearized model.
    - 'chem.inp': Input file for TChem model.
    - 'kmod.echo, kmod.err, kmod.list, kmod.out, kmod.reactions, math_elem.dat, math_nasapol7.dat, math_reac.dat, math_trdbody.dat, math_spec.dat': Extraneous output from TChem.
    - 'pyTChemOneJacobian.py, pyTChemOneJacobian.sh, pyTChemOneJacobian.txt': Scripts and output for Jacobian linearized model of 0-dimensional homogenous gas reactor.